# Details

Date : 2021-07-01 10:53:07

Directory e:\GameServer

Total : 31 files,  895 codes, 27 comments, 183 blanks, all 1105 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [UnitTest/RecordControllerTest.cs](/UnitTest/RecordControllerTest.cs) | C# | 64 | 10 | 17 | 91 |
| [UnitTest/UnitTest.csproj](/UnitTest/UnitTest.csproj) | XML | 22 | 0 | 6 | 28 |
| [UnitTest/UserControllerTest.cs](/UnitTest/UserControllerTest.cs) | C# | 42 | 6 | 14 | 62 |
| [gameserverintern/.dockerignore](/gameserverintern/.dockerignore) | Ignore | 25 | 0 | 1 | 26 |
| [gameserverintern/Constants/ErrorCodeConstant.cs](/gameserverintern/Constants/ErrorCodeConstant.cs) | C# | 13 | 0 | 3 | 16 |
| [gameserverintern/Controllers/RecordController.cs](/gameserverintern/Controllers/RecordController.cs) | C# | 83 | 4 | 18 | 105 |
| [gameserverintern/Controllers/UserController.cs](/gameserverintern/Controllers/UserController.cs) | C# | 61 | 0 | 14 | 75 |
| [gameserverintern/Controllers/WeatherForecastController.cs](/gameserverintern/Controllers/WeatherForecastController.cs) | C# | 35 | 0 | 5 | 40 |
| [gameserverintern/Dockerfile](/gameserverintern/Dockerfile) | Docker | 19 | 2 | 6 | 27 |
| [gameserverintern/GameServer.csproj](/gameserverintern/GameServer.csproj) | XML | 10 | 0 | 4 | 14 |
| [gameserverintern/Interfaces/IRecordRepository.cs](/gameserverintern/Interfaces/IRecordRepository.cs) | C# | 13 | 0 | 2 | 15 |
| [gameserverintern/Interfaces/IRecordService.cs](/gameserverintern/Interfaces/IRecordService.cs) | C# | 15 | 0 | 2 | 17 |
| [gameserverintern/Interfaces/IUserRepository.cs](/gameserverintern/Interfaces/IUserRepository.cs) | C# | 14 | 0 | 3 | 17 |
| [gameserverintern/Interfaces/IUserService.cs](/gameserverintern/Interfaces/IUserService.cs) | C# | 14 | 0 | 4 | 18 |
| [gameserverintern/Models/GameServerDatabaseSettings.cs](/gameserverintern/Models/GameServerDatabaseSettings.cs) | C# | 17 | 0 | 4 | 21 |
| [gameserverintern/Models/Record.cs](/gameserverintern/Models/Record.cs) | C# | 25 | 0 | 2 | 27 |
| [gameserverintern/Models/ResponseModel.cs](/gameserverintern/Models/ResponseModel.cs) | C# | 10 | 0 | 2 | 12 |
| [gameserverintern/Models/User.cs](/gameserverintern/Models/User.cs) | C# | 16 | 0 | 4 | 20 |
| [gameserverintern/Program.cs](/gameserverintern/Program.cs) | C# | 24 | 0 | 3 | 27 |
| [gameserverintern/Properties/launchSettings.json](/gameserverintern/Properties/launchSettings.json) | JSON | 31 | 0 | 1 | 32 |
| [gameserverintern/README.md](/gameserverintern/README.md) | Markdown | 1 | 0 | 1 | 2 |
| [gameserverintern/Repositories/RecordRepository.cs](/gameserverintern/Repositories/RecordRepository.cs) | C# | 39 | 0 | 5 | 44 |
| [gameserverintern/Repositories/UserRepository.cs](/gameserverintern/Repositories/UserRepository.cs) | C# | 41 | 0 | 8 | 49 |
| [gameserverintern/Services/RecordService.cs](/gameserverintern/Services/RecordService.cs) | C# | 67 | 0 | 14 | 81 |
| [gameserverintern/Services/UserService.cs](/gameserverintern/Services/UserService.cs) | C# | 28 | 1 | 6 | 35 |
| [gameserverintern/Startup.cs](/gameserverintern/Startup.cs) | C# | 78 | 2 | 17 | 97 |
| [gameserverintern/WeatherForecast.cs](/gameserverintern/WeatherForecast.cs) | C# | 11 | 0 | 5 | 16 |
| [gameserverintern/appsettings.Development.json](/gameserverintern/appsettings.Development.json) | JSON | 9 | 0 | 1 | 10 |
| [gameserverintern/appsettings.json](/gameserverintern/appsettings.json) | JSON | 19 | 0 | 3 | 22 |
| [gameserverintern/docker-compose.debug.yml](/gameserverintern/docker-compose.debug.yml) | YAML | 13 | 1 | 3 | 17 |
| [gameserverintern/docker-compose.yml](/gameserverintern/docker-compose.yml) | YAML | 36 | 1 | 5 | 42 |

[summary](results.md)