# Summary

Date : 2021-07-01 10:53:07

Directory e:\GameServer

Total : 31 files,  895 codes, 27 comments, 183 blanks, all 1105 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 21 | 710 | 23 | 152 | 885 |
| JSON | 3 | 59 | 0 | 5 | 64 |
| YAML | 2 | 49 | 2 | 8 | 59 |
| XML | 2 | 32 | 0 | 10 | 42 |
| Ignore | 1 | 25 | 0 | 1 | 26 |
| Docker | 1 | 19 | 2 | 6 | 27 |
| Markdown | 1 | 1 | 0 | 1 | 2 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 31 | 895 | 27 | 183 | 1,105 |
| UnitTest | 3 | 128 | 16 | 37 | 181 |
| gameserverintern | 28 | 767 | 11 | 146 | 924 |
| gameserverintern\Constants | 1 | 13 | 0 | 3 | 16 |
| gameserverintern\Controllers | 3 | 179 | 4 | 37 | 220 |
| gameserverintern\Interfaces | 4 | 56 | 0 | 11 | 67 |
| gameserverintern\Models | 4 | 68 | 0 | 12 | 80 |
| gameserverintern\Properties | 1 | 31 | 0 | 1 | 32 |
| gameserverintern\Repositories | 2 | 80 | 0 | 13 | 93 |
| gameserverintern\Services | 2 | 95 | 1 | 20 | 116 |

[details](details.md)