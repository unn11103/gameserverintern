namespace GameServer.Models
{
    public class GameServerDatabaseSettings : IGameServerDatabaseSettings
    {
        public string UsersCollectionName { get; set; }
        public string StagesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IGameServerDatabaseSettings
    {
        public string UsersCollectionName { get; set; }
        public string StagesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}