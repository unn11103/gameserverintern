using System;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace GameServer.Models
{
    public class StageModel
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid PlayerId { get; set; }

        public bool IsStart { get; set; }
        public List<StageInfo> CompletedStage { get; set; }
        public int CurrentStage { get; set; }
        public double Coin { get; set; }
    }
}