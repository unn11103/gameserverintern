namespace GameServer.Models
{
    public class StageInfo
    {
        public int HighScore { get; set; }
        public int StageId { get; set; }
    }
}