using System;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson.Serialization.Attributes;

namespace GameServer.Models
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [BsonElement("Name")]
        public string UserName { get; set; }
    }
}