using System;

namespace GameServer.Models
{
    public class InStage
    {
        public Guid UserId { get; set; }
        public int HighScore { get; set; }
    }
}