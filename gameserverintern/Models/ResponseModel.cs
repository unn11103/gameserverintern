namespace GameServer.Models
{
    public class ResponseModel<T>
    {
        public T PayLoad { get; set; }
        public int ErrorCode { get; set; }
        public bool IsSuccess { get; set; }
    }
}