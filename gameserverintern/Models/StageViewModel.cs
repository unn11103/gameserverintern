using GameServer.Models;
using System;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace GameServer.Models
{
    public class StageViewModel
    {
        public Guid PlayerId { get; set; }
        public bool IsStart { get; set; }
        public List<StageInfo> CompletedStage { get; set; }
        public int CurrentStage { get; set; }
        public double Coin { get; set; }
    }
}