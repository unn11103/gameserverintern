﻿using GameServer.Models;
using System;
using System.Collections.Generic;

namespace GameServer.Interfaces
{
    public interface IUserRepository
    {
        public List<User> GetAllUser();
        public User GetUser(Guid id);
        public User GetUser(string name);
        public bool IsUserValid(String name);
        public User Create(User user);
    }
}