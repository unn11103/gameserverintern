using GameServer.Models;
using System;

namespace GameServer.Interfaces
{
    public interface IStageRepository
    {
        public StageModel Create(StageModel stage);
        public StageModel Get(Guid id);
        public bool IsStart(Guid id);
        public void Update(Guid id, StageModel stage);
    }
}