using System;
using System.Collections.Generic;
using GameServer.Models;

namespace GameServer.Interfaces
{
    public interface IUserService
    {
        public List<User> Get();
        public User Get(Guid id);
        public User Get(string name);
        public bool IsUserValid(String name);
        public User Create(User user);
    }
}