using System;
using GameServer.Models;

namespace GameServer.Interfaces
{
    public interface IStageService
    {
        public StageViewModel Create(StageModel stageModel);
        public StageViewModel Finish(InStage inStage);
        public StageViewModel Get(Guid playerId);
        public StageViewModel Start(Guid playerId, int stageId);
        public void Update(Guid playerId, StageModel stage);
        public bool IsStart(Guid playerId);
    }
}