using System;
using System.Collections.Generic;
using GameServer.Interfaces;
using GameServer.Models;
using MongoDB.Driver;

namespace GameServer.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoDatabase _mongoDb;
        private readonly IGameServerDatabaseSettings _settings;

        public UserRepository(IMongoDatabase mongoDb, IGameServerDatabaseSettings settings)
        {
            _settings = settings;
            _mongoDb = mongoDb;
        }

        public List<User> GetAllUser()
        {
            return _mongoDb.GetCollection<User>(_settings.UsersCollectionName).Find(user => true).ToList();
        }

        public User GetUser(Guid id)
        {
            return _mongoDb.GetCollection<User>(_settings.UsersCollectionName).Find(user => user.Id.Equals(id))
                .FirstOrDefault();
        }

        public User GetUser(string name)
        {
            return _mongoDb.GetCollection<User>(_settings.UsersCollectionName).Find(user => user.UserName.Equals(name))
                .FirstOrDefault();
        }

        public bool IsUserValid(string name)
        {
            var user = _mongoDb.GetCollection<User>(_settings.UsersCollectionName)
                .Find(user => user.UserName.Equals(name)).FirstOrDefault();
            if (user != null) return false;
            return true;
        }

        public User Create(User user)
        {
            _mongoDb.GetCollection<User>(_settings.UsersCollectionName).InsertOne(user);
            return user;
        }
    }
}