using System;
using GameServer.Interfaces;
using GameServer.Models;
using MongoDB.Driver;

namespace GameServer.Repositories
{
    public class StageRepository : IStageRepository
    {
        private readonly IMongoDatabase _mongoDb;
        private readonly IGameServerDatabaseSettings _settings;

        public StageRepository(IMongoDatabase mongoDb, IGameServerDatabaseSettings settings)
        {
            _settings = settings;
            _mongoDb = mongoDb;
        }

        public StageModel Create(StageModel stageModel)
        {
            _mongoDb.GetCollection<StageModel>(_settings.StagesCollectionName).InsertOne(stageModel);
            return stageModel;
        }

        public StageModel Get(Guid id)
        {
            return _mongoDb.GetCollection<StageModel>(_settings.StagesCollectionName)
                .Find(stage => stage.PlayerId.Equals(id)).FirstOrDefault();
        }

        public bool IsStart(Guid id)
        {
            var stageModel = Get(id);
            return stageModel.IsStart;
        }

        public void Update(Guid id, StageModel stageModel)
        {
            _mongoDb.GetCollection<StageModel>(_settings.StagesCollectionName)
                .ReplaceOne(stage => stage.PlayerId == id, stageModel);
        }
    }
}