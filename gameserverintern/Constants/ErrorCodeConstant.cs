﻿namespace GameServer.Constants
{
    public static class ErrorCodeConstant
    {
        public const int AlreadyExists = 4000;
        public const int NotFound = 1001;
        public const int InvalidGame = 5000;
    }
}