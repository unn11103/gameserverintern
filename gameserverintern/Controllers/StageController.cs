using System;
using GameServer.Constants;
using GameServer.Interfaces;
using GameServer.Models;
using Microsoft.AspNetCore.Mvc;

namespace GameServer.Controllers
{
    [ApiController]
    [Route("stage")]
    public class StageController : ControllerBase
    {
        private readonly IStageService _stageService;

        public StageController(IStageService stageService)
        {
            _stageService = stageService;
        }

        [HttpGet]
        [Route("get")]
        public ActionResult<StageViewModel> Get(Guid playerId)
        {
            return _stageService.Get(playerId);
        }

        [HttpPost]
        [Route("start")]
        public ActionResult<ResponseModel<StageViewModel>> Start(Guid playerId, int stageId)
        {
            var response = new ResponseModel<StageViewModel>();
            var stageView = _stageService.Start(playerId, stageId);

            if (stageView == null)
            {
                // Starting first time on the stage that is not first stage.
                response.IsSuccess = false;
                response.ErrorCode = ErrorCodeConstant.InvalidGame;
                return response;
            }

            // Success
            response.IsSuccess = true;
            response.PayLoad = stageView;
            return response;
        }

        [HttpPut]
        [Route("finish")]
        public ActionResult<ResponseModel<StageViewModel>> Finish(InStage inStage)
        {
            var response = new ResponseModel<StageViewModel>();
            var stageViewModel = _stageService.Get(inStage.UserId);

            if (stageViewModel == null)
            {
                response.IsSuccess = false;
                response.ErrorCode = ErrorCodeConstant.NotFound;
                return response;
            }

            // Finish game without start.
            if (stageViewModel.IsStart == false)
            {
                response.IsSuccess = false;
                response.ErrorCode = ErrorCodeConstant.InvalidGame;
                response.PayLoad = stageViewModel;
                return response;
            }

            stageViewModel = _stageService.Finish(inStage);
            response.IsSuccess = true;
            response.PayLoad = stageViewModel;
            return response;
        }
    }
}