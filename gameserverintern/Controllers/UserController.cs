using GameServer.Interfaces;
using GameServer.Models;
using GameServer.Constants;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace GameServer.Controllers
{
    [ApiController]
    [Route("api")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userServices;

        public UserController(IUserService userService)
        {
            _userServices = userService;
        }

        [HttpGet]
        [Route("get_user")]
        public ActionResult<List<User>> Get()
        {
            return _userServices.Get();
        }

        [HttpGet]
        [Route("get_user/{id}", Name = "GetUser")]
        public ActionResult<User> Get(Guid id)
        {
            Console.WriteLine(id);
            var user = _userServices.Get(id);
            if (user == null)
            {
                return BadRequest(ErrorCodeConstant.NotFound);
            }

            return user;
        }

        [HttpPost]
        [Route("register_user")]
        public ActionResult<ResponseModel<User>> Create(User user)
        {
            var response = new ResponseModel<User>();
            var createResponse = new User();

            var existsUser = _userServices.Get(user.UserName);
            if (existsUser != null)
            {
                response.ErrorCode = ErrorCodeConstant.AlreadyExists;
                response.IsSuccess = false;
                createResponse.Id = existsUser.Id;
                createResponse.UserName = existsUser.UserName;
                response.PayLoad = createResponse;
                return response;
            }

            _userServices.Create(user);
            createResponse.Id = user.Id;
            response.IsSuccess = true;
            createResponse.UserName = user.UserName;
            response.PayLoad = createResponse;
            return response;
        }
    }
}