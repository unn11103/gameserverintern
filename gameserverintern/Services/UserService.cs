using System;
using System.Collections.Generic;
using GameServer.Interfaces;
using GameServer.Models;

namespace GameServer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<User> Get()
        {
            return _userRepository.GetAllUser();
        }

        public User Get(Guid id)
        {
            return _userRepository.GetUser(id);
        }

        public User Get(string name)
        {
            return _userRepository.GetUser(name);
        }

        public bool IsUserValid(string name)
        {
            return _userRepository.IsUserValid(name);
        }

        public User Create(User user)
        {
            return _userRepository.Create(user);
        }
    }
}