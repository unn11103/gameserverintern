using System;
using System.Collections.Generic;
using System.Linq;
using GameServer.Interfaces;
using GameServer.Models;

namespace GameServer.Services
{
    public class StageService : IStageService
    {
        private readonly IStageRepository _stageRepository;

        public StageService(IStageRepository stageRepository)
        {
            _stageRepository = stageRepository;
        }

        public StageViewModel Create(StageModel stage)
        {
            return GetStageViewModel(_stageRepository.Create(stage));
        }

        public StageViewModel Get(Guid playerId)
        {
            return GetStageViewModel(_stageRepository.Get(playerId));
        }

        public bool IsStart(Guid playerId)
        {
            return _stageRepository.IsStart(playerId);
        }

        public StageViewModel Start(Guid playerId, int stageId)
        {
            var stage = _stageRepository.Get(playerId);
            if (stage == null)
            {
                if (stageId == 0)
                {
                    var newStage = new StageModel
                    {
                        PlayerId = playerId,
                        IsStart = true,
                        Coin = 0,
                        CurrentStage = 0,
                        CompletedStage = new List<StageInfo>()
                    };
                    _stageRepository.Create(newStage);

                    return GetStageViewModel(newStage);
                }

                return null;
            }

            if (stage.CompletedStage.Count() >= stageId || stageId == 0)
            {
                stage.IsStart = true;
                stage.CurrentStage = stageId;
                _stageRepository.Update(stage.PlayerId, stage);

                return GetStageViewModel(stage);
            }

            return null;
        }

        public StageViewModel Finish(InStage inStage)
        {
            var stage = _stageRepository.Get(inStage.UserId);
            stage.IsStart = false;
            var stageInfo = new StageInfo
            {
                StageId = stage.CurrentStage,
                HighScore = inStage.HighScore
            };

            // If finishing the stage for the first time else update exists.
            if (stage.CompletedStage.Count() <= stage.CurrentStage || stage.CompletedStage.Count() == 0)
            {
                stage.CompletedStage.Add(stageInfo);
            }
            else
            {
                // Update new high score.
                if (inStage.HighScore > stage.CompletedStage[stage.CurrentStage].HighScore)
                    stage.CompletedStage[stage.CurrentStage].HighScore = inStage.HighScore;
            }

            // Add coin to the player.
            if(inStage.HighScore <= 100)
            {
                stage.Coin += inStage.HighScore;
            }
            else if (inStage.HighScore >= 100)
            {
                if (inStage.HighScore <= 500 && inStage.HighScore > 100)
                {
                    stage.Coin += inStage.HighScore * 2;
                }
                else if (inStage.HighScore > 500)
                {
                    stage.Coin += inStage.HighScore * 10;
                }
            }

            _stageRepository.Update(stage.PlayerId, stage);
            return GetStageViewModel(stage);
        }

        public void Update(Guid playerId, StageModel stage)
        {
            _stageRepository.Update(playerId, stage);
        }

        private StageViewModel GetStageViewModel(StageModel stage)
        {
            var stageViewModel = new StageViewModel();
            stageViewModel.PlayerId = stage.PlayerId;
            stageViewModel.IsStart = stage.IsStart;
            stageViewModel.Coin = stage.Coin;
            stageViewModel.CurrentStage = stage.CurrentStage;
            stageViewModel.CompletedStage = stage.CompletedStage;
            return stageViewModel;
        }
    }
}