using GameServer.Interfaces;
using GameServer.Models;
using GameServer.Services;
using GameServer.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;

namespace GameServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<GameServerDatabaseSettings>(
                Configuration.GetSection(nameof(GameServerDatabaseSettings))
            );

            services.AddSingleton<IGameServerDatabaseSettings>(sp => 
                sp.GetRequiredService<IOptions<GameServerDatabaseSettings>>().Value);

            services.AddSingleton<IUserService,UserService>();
            services.AddSingleton<IUserRepository, UserRepository>();

            services.AddSingleton<IStageService,StageService>();
            services.AddSingleton<IStageRepository, StageRepository>();

            services.AddControllers();
            
            var settings = new GameServerDatabaseSettings();
            ConfigureMongoDb(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "GameServer", Version = "v1" });
            });
        }

        private void ConfigureMongoDb(IServiceCollection services)
        {
            var settings = GetGameServerDatabaseSettings();
            var client = new MongoClient(settings.ConnectionString);
            var db = client.GetDatabase(settings.DatabaseName);
            var userCollection = db.GetCollection<User>(settings.UsersCollectionName);
            var stageCollection = db.GetCollection<User>(settings.StagesCollectionName);
            services.AddSingleton(db);
            services.AddSingleton(userCollection);
            services.AddSingleton(stageCollection);
        }

        private IMongoDatabase CreateMongoDatabase(IGameServerDatabaseSettings settings)
        {
            var client = new MongoClient(Configuration.GetConnectionString("MongoDb"));
            return client.GetDatabase(settings.DatabaseName);
        }

        private IGameServerDatabaseSettings GetGameServerDatabaseSettings()
        {
            return Configuration.GetSection(nameof(GameServerDatabaseSettings)).Get<GameServerDatabaseSettings>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "GameServer v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
