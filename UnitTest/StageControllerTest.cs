﻿using System;
using System.Collections.Generic;
using GameServer.Constants;
using GameServer.Controllers;
using GameServer.Interfaces;
using GameServer.Models;
using Moq;
using Xunit;

namespace UnitTest
{
    public class StageControllerTest
    {
        [Fact]
        public void Finish_game_without_start_return_error()
        {
            // Arrange
            var mockService = new Mock<IStageService>();
            var guid = new Guid();
            var inStage = new InStage {UserId = guid, HighScore = 350};
            var stage = new StageModel {PlayerId = guid, Coin = 0, IsStart = false, CurrentStage = 0};
            var stageView = new StageViewModel
            {
                PlayerId = stage.PlayerId,
                Coin = stage.Coin,
                IsStart = stage.IsStart,
                CurrentStage = stage.CurrentStage
            };
            mockService.Setup(service => service.Get(stage.PlayerId)).Returns(stageView);

            var controller = new StageController(mockService.Object);

            // Act
            var result = controller.Finish(inStage);

            // Assert
            Assert.Equal(ErrorCodeConstant.InvalidGame, result.Value.ErrorCode);
        }

        [Fact]
        public void Finish_non_existing_game_return_error()
        {
            var mockService = new Mock<IStageService>();
            var guid = new Guid();
            var inStage = new InStage {UserId = guid, HighScore = 350};
            var stage = new StageModel {PlayerId = guid, Coin = 0, IsStart = false, CurrentStage = 0};
            mockService.Setup(service => service.Get(stage.PlayerId)).Returns((StageViewModel) null);
            var controller = new StageController(mockService.Object);

            // Act
            var result = controller.Finish(inStage);

            // Assert
            Assert.Equal(ErrorCodeConstant.NotFound, result.Value.ErrorCode);
        }

        [Fact]
        public void Start_without_completing_prev_stage_return_error()
        {
            // Arrange
            var mockService = new Mock<IStageService>();
            var guid = new Guid();
            mockService.Setup(service => service.Start(guid, 0)).Returns((StageViewModel) null);
            var controller = new StageController(mockService.Object);

            // Act
            var result = controller.Start(guid, 0);

            // Assert
            Assert.Equal(ErrorCodeConstant.InvalidGame, result.Value.ErrorCode);
        }

        [Fact]
        public void Start_game_with_exists_user_and_stage_return_true()
        {
            var mockService = new Mock<IStageService>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = true,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo>()
            };
            var stageView = new StageViewModel
            {
                PlayerId = stage.PlayerId,
                Coin = stage.Coin,
                IsStart = stage.IsStart,
                CurrentStage = stage.CurrentStage,
                CompletedStage = stage.CompletedStage
            };
            mockService.Setup(service => service.Get(guid)).Returns(stageView);
            mockService.Setup(service => service.Start(guid, 0)).Returns(stageView);
            var controller = new StageController(mockService.Object);

            // Act
            var result = controller.Start(guid, 0);

            // Assert
            Assert.True(result.Value.IsSuccess);
        }

        [Fact]
        public void Start_new_completed_stage_without_finish_current_stage_return_true()
        {
            var mockService = new Mock<IStageService>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = true,
                CurrentStage = 1,
                CompletedStage = new List<StageInfo> {new() {HighScore = 350, StageId = 0}}
            };
            var stageView = new StageViewModel
            {
                PlayerId = stage.PlayerId,
                Coin = stage.Coin,
                IsStart = stage.IsStart,
                CurrentStage = stage.CurrentStage,
                CompletedStage = stage.CompletedStage
            };

            mockService.Setup(service => service.Start(guid, 0)).Returns(stageView);
            var controller = new StageController(mockService.Object);

            // Act
            var result = controller.Start(guid, 0);

            // Assert
            Assert.True(result.Value.IsSuccess);
        }
    }
}