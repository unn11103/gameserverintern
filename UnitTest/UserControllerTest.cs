using System;
using Xunit;
using Moq;
using GameServer.Interfaces;
using GameServer.Controllers;
using GameServer.Models;
using GameServer.Constants;


namespace UnitTest
{
    public class UserControllerTest
    {
        [Fact]
        public void Create_Exists_Username_Returns_Error()
        {
            // Arrange
            Mock<IUserService> mockService = new Mock<IUserService>();
            var guid = Guid.NewGuid();
            var user = new User {Id = guid, UserName = "user1"};
            var name = "user1";

            mockService.Setup(service => service.Get(name)).Returns(user);
            var controller = new UserController(mockService.Object);

            // Act
            var result = controller.Create(user);

            // Assert
            Assert.Equal(result.Value.ErrorCode, ErrorCodeConstant.AlreadyExists);
        }

        [Fact]
        public void Create_User_Returns_Guid()
        {
            // Arrange
            Mock<IUserService> mockService = new Mock<IUserService>();

            var guid = Guid.NewGuid();
            var user = new User {Id = guid, UserName = "user1"};
            var name = "user1";
            mockService.Setup(mockRepo => mockRepo.IsUserValid(name)).Returns(true);
            mockService.Setup(service => service.Create(user)).Returns(user);
            var controller = new UserController(mockService.Object);

            // Act

            var result = controller.Create(user);

            // Assert

            Assert.Equal(result.Value.PayLoad.Id, guid);
        }
    }
}