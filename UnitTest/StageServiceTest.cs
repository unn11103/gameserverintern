﻿using System;
using Xunit;
using Moq;
using GameServer.Interfaces;
using GameServer.Models;
using System.Collections.Generic;
using GameServer.Services;

namespace UnitTest
{
    public class StageServiceTest
    {
        [Fact]
        public void Create_method_return_stage()
        {
            // Arrange
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel {PlayerId = guid, Coin = 0, IsStart = false, CurrentStage = 0};
            mockrepo.Setup(repo => repo.Create(stage)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Create(stage);

            // Assert
            Assert.Equal(guid, result.PlayerId);
        }

        [Fact]
        public void Get_stage_by_id_return_stage()
        {
            // Arrange
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel {PlayerId = guid, Coin = 0, IsStart = false, CurrentStage = 0};
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Get(stage.PlayerId);

            // Assert
            Assert.Equal(stage.PlayerId, result.PlayerId);
        }

        [Fact]
        public void Start_game_with_correct_stage_return_stage()
        {
            // Arrange
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = false,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo> {new StageInfo {StageId = 0, HighScore = 350}}
            };
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Start(stage.PlayerId, 1);

            // Assert
            Assert.Equal(guid, result.PlayerId);
        }

        [Fact]
        public void Start_game_first_time_with_stage0_return_stage()
        {
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = false,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo> {new StageInfo {StageId = 0, HighScore = 350}}
            };
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns((StageModel) null);
            mockrepo.Setup(repo => repo.Create(stage)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Start(stage.PlayerId, 0);

            // Assert
            Assert.Equal(guid, result.PlayerId);
        }

        [Fact]
        public void Start_game_first_time_without_stage0_return_null()
        {
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = false,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo> {new StageInfo {StageId = 0, HighScore = 350}}
            };
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns((StageModel) null);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Start(stage.PlayerId, 1);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void Start_game_with_incorrect_stage_return_null()
        {
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = false,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo> {new StageInfo {StageId = 0, HighScore = 350}}
            };
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Start(stage.PlayerId, 2);

            // Assert
            Assert.Null(result);
        }
        
        [Fact]
        public void Finish_game_high_score_0_added_coin()
        {
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = false,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo>()
            };
            var inStage = new InStage { UserId = guid, HighScore = 0 };
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Finish(inStage);

            // Assert
            Assert.Equal(0, result.Coin);
        }

        [Fact]
        public void Finish_game_high_score_100_added_coin()
        {
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = false,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo>()
            };
            var inStage = new InStage { UserId = guid, HighScore = 100 };
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Finish(inStage);

            // Assert
            Assert.Equal(100, result.Coin);
        }

        [Fact]
        public void Finish_game_high_score_500_added_coin()
        {
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = false,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo>()
            };
            var inStage = new InStage { UserId = guid, HighScore = 500 };
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Finish(inStage);

            // Assert
            Assert.Equal(1000, result.Coin);
        }

        [Fact]
        public void Finish_game_high_score_1000_added_coin()
        {
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = false,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo>()
            };
            var inStage = new InStage { UserId = guid, HighScore = 1000 };
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Finish(inStage);

            // Assert
            Assert.Equal(10000, result.Coin);
        }


        [Fact]
        public void Finish_game_return_stage()
        {
            Mock<IStageRepository> mockrepo = new Mock<IStageRepository>();
            var guid = new Guid();
            var stage = new StageModel
            {
                PlayerId = guid,
                Coin = 0,
                IsStart = false,
                CurrentStage = 0,
                CompletedStage = new List<StageInfo>()
            };
            var inStage = new InStage {UserId = guid, HighScore = 350};
            mockrepo.Setup(repo => repo.Get(stage.PlayerId)).Returns(stage);
            var service = new StageService(mockrepo.Object);

            // Act 
            var result = service.Finish(inStage);

            // Assert
            Assert.Equal(stage.PlayerId, result.PlayerId);
        }
    }
}